<?php
namespace App\Controllers;

use \App\Models\Jugador;
use \App\Models\Puesto;

class JugadorController
{

    function __construct()
    {

    }

    public function index()
    {
        $pagesize = 5;
        $jugadores = Jugador::paginate($pagesize);
        $rowCount = Jugador::rowCount();

        $pages = ceil($rowCount / $pagesize);
        if (isset($_REQUEST['page'])) {
            $page = (integer) $_REQUEST['page'];
        } else {
            $page = 1;
        }
        $tipoJugador = Puesto::all();

        require "../app/views/jugadores/index.php";
    }
    public function viewRegister(){
        $puesto = Puesto::all();
        require "../app/views/jugadores/create.php";
    }
    public function store(){
        $jugador = new Jugador();

        $jugador->nombre = $_REQUEST["name"];
        $jugador->id_puesto = $_REQUEST["type"];
        $jugador->nacimiento = $_REQUEST["age"];

        $jugador->insert();

        header('Location:/jugador');
    }
}
