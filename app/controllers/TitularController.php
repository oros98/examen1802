<?php
namespace App\Controllers;

use \App\Models\Jugador;
use \App\Models\Puesto;

class TitularController
{

    function __construct()
    {

    }

    public function index()
    {
        if(isset($_SESSION["listaTitulares"])){
            $jugadores = $_SESSION["listaTitulares"];
        }else{
            $jugadores = [];
        }
        $tipoJugador = Puesto::all();

        require "../app/views/titulares/index.php";
    }
    public function add($arguments){
         if(isset($_SESSION["listaTitulares"])){
            $jugadores = $_SESSION["listaTitulares"];
        }else{
            $jugadores = [];
        }
        $id = (int)$arguments[0];
        $jugador= Jugador::find($id);
        foreach ($jugadores as $key => $value) {
            if($value->id==$id){
                header("Location: /titular");
                return;
            }
        }
        $jugadores[]=$jugador;
        $_SESSION["listaTitulares"]=$jugadores;
        header("Location: /titular");

    }
    public function remove($arguments){
        $id = (int)$arguments[0];
        $jugadores = $_SESSION["listaTitulares"];
         foreach ($jugadores as $key => $value) {
            if($value->id==$id){
                unset($jugadores[$key]);
            }
        }
        $_SESSION["listaTitulares"]=$jugadores;
        header("Location: /titular");
    }
}
