<?php
namespace App\Models;

use PDO;
use Core\Model;

// require_once '../core/Model.php';
/**
*
*/
class Jugador extends Model
{

    function __construct()
    {

    }

    public function paginate($size = 10)
    {
        if (isset($_REQUEST['page'])) {
            $page = (integer) $_REQUEST['page'];
        } else {
            $page = 1;
        }

        $offset = ($page - 1) * $size;

        // var_dump($size);
        // // echo "tamaño: $size<br>";
        // // echo "En la página número $page";
        // // echo " empezamos en el registro $offset";
        // exit();

        $db = Jugador::db();
        $statement = $db->prepare('SELECT * FROM jugadores LIMIT :pagesize OFFSET :offset');
        $statement->bindValue(':pagesize', $size, PDO::PARAM_INT);
        $statement->bindValue(':offset', $offset, PDO::PARAM_INT);
        $statement->execute();

        $jugadores = $statement->fetchAll(PDO::FETCH_CLASS, Jugador::class);

        return $jugadores;
    }

    public static function rowCount()
    {
        $db = Jugador::db();
        $statement = $db->prepare('SELECT count(id) as count FROM jugadores');
        $statement->execute();

        $rowCount = $statement->fetch(PDO::FETCH_ASSOC);
        return $rowCount['count'];
    }
    public function insert() {

        $db=Jugador::db();

        $statement = $db->prepare('INSERT INTO jugadores (nombre, nacimiento, id_puesto)
            VALUES(:nombre, :age, :id_puesto)');
        $statement->bindValue(':nombre',$this->nombre);
        $statement->bindValue(':age',date($this->nacimiento));
        $statement->bindValue(':id_puesto',$this->id_puesto);

        return $statement->execute();
    }
     public static function find($id){
       $db=Jugador::db();

        $statement = $db->prepare('SELECT * FROM jugadores WHERE id =?');
        $statement->bindValue(1,$id,PDO::PARAM_INT);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_CLASS,Jugador::class);
        $jugadores = $statement->fetch(PDO::FETCH_CLASS);
        return $jugadores;
    }
}
