<?php
namespace App\Models;

use PDO;
use Core\Model;

// require_once '../core/Model.php';
/**
*
*/
class Puesto extends Model
{

    function __construct()
    {

    }
    public static function all(){


        $db=Puesto::db();

        $statement = $db->query('SELECT * FROM puestos');
        $puesto = $statement->fetchAll(PDO::FETCH_CLASS,Puesto::class);
        return $puesto;
    }
}
