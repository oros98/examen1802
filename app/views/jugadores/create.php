<!doctype html>
<html lang="es">
<head>
  <?php require "../app/views/parts/head.php" ?>
</head>
<body>

  <?php require "../app/views/parts/header.php" ?>

  <main role="main" class="container">
    <div class="starter-template">
      <h1>Alta de Jugador</h1>

      <form method="post" action="/jugador/store">

        <div class="form-group">
          <label>Nombre</label>
          <input type="text" name="name" class="form-control">
        </div>
        <div class="form-group">
          <label>Puesto</label>
          <select class="form-control" name="type">
           <?php foreach ($puesto as $key => $valuetype): ?>
              <option value="<?php echo $valuetype->id?>"><?php echo $valuetype->nombre?></option>
            <?php endforeach?>
          </select>
        </div>
        <div class="form-group">
          <label>Edad</label>
          <input type="text" name="age" class="form-control">
        </div>
        <button type="submit" class="btn btn-default">Enviar</button>
      </form>
    </div>
  </main><!-- /.container -->
  <?php require "../app/views/parts/footer.php" ?>


</body>
  <?php require "../app/views/parts/scripts.php" ?>
</html>
