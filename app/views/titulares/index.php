<!doctype html>
<html lang="es">
<head>
  <?php require "../app/views/parts/head.php" ?>
</head>
<body>

  <?php require "../app/views/parts/header.php" ?>

  <main role="main" class="container">
    <div class="starter-template">
      <h1>Lista de Jugadores</h1>

    <table class="table table-striped">
      <?php if(count($jugadores)>0){?>
      <thead>
        <tr>
          <th>Id</th>
          <th>Nombre</th>
          <th>Puesto</th>
          <th>Nacimiento</th>
          <th>Operaciones</th>
        </tr>
      </thead>
      <tbody>
        <?php foreach ($jugadores as $jugador): ?>
          <tr>
          <td><?php echo $jugador->id ?></td>
          <td><?php echo $jugador->nombre ?></td>
          <?php foreach ($tipoJugador as $valuetype):
              if($jugador->id_puesto==$valuetype->id){ ?>
              <td><?php echo $valuetype->nombre; break;?></td>
              <?php }?>
            <?php endforeach?>
          <td><?php echo date("d-m-Y", strtotime($jugador->nacimiento)) ?></td>
          <td>
            <a class="btn btn-primary" href="/titular/remove/<?php echo $jugador->id ?>">Quitar</a>
          </td>
          </tr>
        <?php endforeach ?>
      </tbody>
              <?php }else{ ?>
          <h2>No hay titulares</h2>
        <?php } ?>
    </table>
    </div>

  </main><!-- /.container -->
  <?php require "../app/views/parts/footer.php" ?>


</body>
  <?php require "../app/views/parts/scripts.php" ?>
</html>
